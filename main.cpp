#include <iostream>
#include <mpi.h>
#include <fstream>
#include <vector>

using namespace std;

int f(int x, int a)
{
   return x * x * x * a - x * x * a * a - a;
}

vector<int> readFromFile() {
    vector<int> here;
    int number;
    ifstream input("../data_files/input.txt");

    while (input >> number) {
        here.push_back(number);
    }

    input.close();
    return here;
}

void writeToFile(vector<int> here) {
    ofstream output("../data_files/output.txt");

    for (const auto elem :here) {
        output << elem << endl;
    }

    output.close();
}

int main(int argc, char **argv)
{
   int rank, world_size, a, negative_sum;
   std::vector<int> date;
   unsigned long array_size;
   double start, end;

   MPI_Init(&argc,&argv);
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   MPI_Comm_size(MPI_COMM_WORLD, &world_size);

   if (rank == 0) {
       cout << "a = ";
       cin >> a;
       date = readFromFile();
   }

   MPI_Barrier(MPI_COMM_WORLD);
   start = MPI_Wtime();

   array_size = date.size();
   MPI_Bcast(&array_size, 1, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);
   MPI_Bcast(&a, 1, MPI_INT, 0, MPI_COMM_WORLD);

   // send +1 more per chunk .. because equally sized buffers are needed
   int dimension = static_cast<int>(array_size / world_size) + 1;
   vector<int> recv_buf;
   recv_buf.resize(static_cast<unsigned long>(dimension));

   MPI_Scatter(date.data(), dimension, MPI_INT, recv_buf.data(), dimension, MPI_INT, 0, MPI_COMM_WORLD);

   int negative = 0;
   for (int i = 0; i < dimension; i++) {
       // skip over empty values added because of equally sized buffers
       if (rank * dimension + i < array_size) {
           recv_buf[i] = f(recv_buf[i], a);
           if (recv_buf[i] < 0) {
               negative++;
           }
       }
   }

   MPI_Reduce(&negative, &negative_sum, 1, MPI_INT, MPI_SUM, 1, MPI_COMM_WORLD);
   MPI_Gather(recv_buf.data(), dimension, MPI_INT, date.data(), dimension, MPI_INT, 0, MPI_COMM_WORLD);

   MPI_Barrier(MPI_COMM_WORLD);
   end = MPI_Wtime();

   if (rank == 0) {
       writeToFile(date);
   }

   if (rank == 1) {
       printf("Total negative: %d\n", negative_sum);
   }


   MPI_Finalize();

   printf("Total time: %lf\n", (end - start) * 1000);
}
